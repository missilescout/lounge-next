"use strict";

module.exports = {
	// ## Server settings

	public: true,

	host: undefined,

	port: 9000,

	bind: undefined,

	reverseProxy: true,

	maxHistory: 10000,

	// ### `https`

	https: {
		enable: true,
		key: "ssl/cert.key",
		certificate: "ssl/cert.pem",
		ca: "ssl/ca.pem",
	},

	// ## Client settings

	theme: "main",

	prefetch: false,

	// ### `disableMediaPreview`
	//
	// When set to `true`, The Lounge will not preview media (images, video and
	// audio) hosted on third-party sites. This ensures the client does not
	// make any requests to external sites. If `prefetchStorage` is enabled,
	// images proxied via the The Lounge will be previewed.
	//
	// This has no effect if `prefetch` is set to `false`.
	//
	// This value is set to `false` by default.
	disableMediaPreview: false,

	// ### `prefetchStorage`

	prefetchStorage: false,

	prefetchMaxImageSize: 2048,

	fileUpload: {
		enable: false,
		maxFileSize: 10240,
		baseUrl: null,
	},

	transports: ["polling", "websocket"],

	leaveMessage: "ScoutLink Webchat - https://webchat.scoutlink.net",

	defaults: {
		name: "ScoutLink",
		host: "",
		port: 6697,
		password: "",
		tls: true,
		rejectUnauthorized: true,
		nick: "Guest%%%%",
		username: "",
		realname: "ScoutLink Webchat",
		join: "",
	},

	displayNetwork: false,

	lockNetwork: true,

	messageStorage: ["sqlite", "text"],

	useHexIp: false,

	// ## WEBIRC support
	//
	webirc: null,

	identd: {
		enable: false,
		port: 113,
	},

	oidentd: null,

	ldap: {
		enable: false,

		url: "ldaps://example.com",

		tlsOptions: {},

		primaryKey: "uid",

		searchDN: {
			rootDN: "cn=thelounge,ou=system-users,dc=example,dc=com",
			rootPassword: "1234",

			filter: "(objectClass=person)(memberOf=ou=accounts,dc=example,dc=com)",

			base: "dc=example,dc=com",
			scope: "sub",
		},
	},

	// Don't enable any of these in prod
	debug: {
		ircFramework: false,

		raw: false,
	},
};
