#!/bin/sh

sed -i "s#webirc: null#webirc: {\"${WEBIRC_HOST}\": \"${WEBIRC_PASS}\"}#g" defaults/config.js
sed -i "s#host: \"\"#host: \"${WEBIRC_HOST}\"#g" defaults/config.js
sed -i "s#realname: \"ScoutLink Webchat\"#realname: \"ScoutLink Webchat - ${WEBCHAT_HOST}\"#g" defaults/config.js

if [ "$1" = "thelounge" -a "$(id -u)" = '0' ]; then
    find "${THELOUNGE_HOME}" \! -user node -exec chown node '{}' +
    exec su node -c "$*"
fi

exec "$@"
